import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public myId = "classId";

  public successClass = "text-success";

  public message="student registation form";

  public Name="";
  public Age="";
  public RollNumber="";
  public Class=""; 

  constructor(){}

  ngOnInit(){

  }
  logMessage(value){
    console.log(value);
  }

  title = 'Rform';
  myClickFunction(event){
    alert ("student data is added");
    console.log(event);
  }
}
